<p align="center">
pacebox-jwt 基于jwt安全令牌的扩展工具包
</p>
<p align="center">
-- 主页：<a href="http://mhuang.tech/pacebox-jwt">http://mhuang.tech/pacebox-jwt</a>  --
</p>
<p align="center">
    -- QQ群①:<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=6703688b236038908f6c89b732758d00104b336a3a97bb511048d6fdc674ca01"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="pacebox官方交流群①" title="pacebox官方交流群①"></a>
</p>
---------------------------------------------------------------------------------------------------------------------------------------------------------

## 简介
pacebox-jwt 是一个基于jwt和pacebox-core封装的便捷工具包、简单几行代码即可对jwt进行操作


## 安装

### MAVEN
在pom.xml中加入
```
    <dependency>
        <groupId>tech.mhuang.pacebox</groupId>
        <artifactId>pacebox-jwt</artifactId>
        <version>${laster.version}</version>
    </dependency>
```
### 非MAVEN
下载任意链接
- [Maven中央库1](https://repo1.maven.org/maven2/tech/mhuang/pacebox/pacebox-jwt/)
- [Maven中央库2](http://repo2.maven.org/maven2/tech/mhuang/pacebox/pacebox-jwt/)
> 注意
> pacebox只支持jdk1.8以上的版本