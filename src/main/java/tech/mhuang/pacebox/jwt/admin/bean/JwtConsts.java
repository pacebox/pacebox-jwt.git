package tech.mhuang.pacebox.jwt.admin.bean;

/**
 * jwt常量类
 *
 * @author mhuang
 * @since 1.0.0
 */
public final class JwtConsts {

    public static final String DEFAULT_CLIENT_ID = "authToken";
    public static final String DEFAULT_SECRET = "YXV0aGVyIGlzIG1odWFuZywgcGFjZWJveCBpcyBnb29kIGZyYW1ld29yayBsaXN0LHBhY2Vib3ggc3VwcG9ydCBhbGwgZnJhbWV3b3Jr";
    public static final String DEFAULT_NAME = "mhuang";
    public static final String DEFAULT_TYPE = "Authorization";
    public static final String DEFAULT_HEADER_NAME = "Bearer";
    public static final Long DEFAULT_EXPIRE_MINUTE = 60 * 24 * 30L;
}
