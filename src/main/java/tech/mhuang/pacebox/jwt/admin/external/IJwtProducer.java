package tech.mhuang.pacebox.jwt.admin.external;

import tech.mhuang.pacebox.jwt.admin.bean.Jwt;

import java.util.Map;

/**
 * jwt生产器
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface IJwtProducer {

    /**
     * 定义名称
     *
     * @param name 名称
     */
    void name(String name);

    /**
     * 添加jwt配置
     *
     * @param jwt 配置
     */
    void add(Jwt.JwtBean jwt);

    /**
     * 获取jwt配置
     *
     * @return jwt配置
     */
    Jwt.JwtBean jwt();

    /**
     * 解析token
     *
     * @param jsonWebToken token
     * @return 返回解析后的map对象
     */
    Map<String, Object> parse(String jsonWebToken);

    /**
     * 刷新token
     *
     * @param claims 通过参数进行刷新
     * @return 返回刷新后的token
     */
    String refresh(Map<String, Object> claims);

    /**
     * 创建token
     *
     * @param claims 传递需要的参数、解析后可获得
     * @return 返回创建后的加密Token
     */
    String create(Map<String, Object> claims);
}
